$(function(){
	$('#container').slider();
});
(function($){
	'use strict';
		
		var privates = {
			start:function(){
				privates.showSlideByIndex(0);
			},
			/**
			 * Cette focntion permet d'afficher le slide par son index
			 */
			showSlideByIndex:function(index) {
				// en fonction de l'index je doit affihcer les boutons suivant
				// ou/et précedent
				if( index === 0) {
					// C'est le premier et j'affiche le next
					
					privates.setNavigationDivs('next',true);
					privates.setNavigationDivs('previous',false);
					privates.animate = false;
				} else if ( index === 3) {
					privates.setNavigationDivs('next',false);
					privates.setNavigationDivs('previous',true);
				} else {
					privates.setNavigationDivs('next',true);
					privates.setNavigationDivs('previous',true);
				}
			
				
				if( privates.animate ) {
					$('#container').children('.slide.active').removeClass('active').animate();
					$($('#container').children('.slide').get(index)).addClass('active').animate();
					privates.animate = true;
				}else {
					$('#container').children('.slide').removeClass('active');
					$($('#container').children('.slide').get(index)).addClass('active');
				}
				$('#container').attr('data-slider-position', index);
			},
			setNavigationDivs:function(what,view){
				if( view ) {
					
					$('#slider-'+what).removeClass('slider-hide');
				} else {
					if(!$('#slider-'+what).hasClass('slider-hide'))
						$('#slider-'+what).addClass('slider-hide');
				}
			},
			createPreviousNextDivs:function(){
				$('<div id="slider-previous" class="slider-hide"><h3>Previous</h3></div>').prependTo('#container');
				$('<div id="slider-next"  class="slider-hide"><h3>Next</h3></div>').appendTo('#container');
				
			}
		};
		var methods = {
			init:function(options){
				console.log('init');
				var settings = $.extend({
					slideClassName:'slide'
				}, options);
				return this.each(function(){
					// je dois récupérer le nombre de slides
					$('#container').attr('data-slider-position', 0);
					$('#container').attr('data-slider-nbslides', $(this).children('.'+settings.slideClassName).length);
					// Je dois créer des classes :
					$(this).children('.'+settings.slideClassName).first().addClass('first');
					$(this).children('.'+settings.slideClassName).last().addClass('last');
					$('#slider-next').live('click', methods.next);
					$('#slider-previous').live('click', methods.previous);
					privates.createPreviousNextDivs();
					privates.start();
				})
			},
			next:function(e){
				var position = parseInt($('#container').attr('data-slider-position'));
				var nbslides = parseInt($('#container').attr('data-slider-nbslides'));
				var nextis = position+1;  
				if( nextis >= nbslides ) {
					console.error('You should not be there ... next');
				} else {					
					privates.showSlideByIndex(nextis);
				}
			},
			previous:function(){
				var position = parseInt($('#container').attr('data-slider-position'));
				var nbslides = parseInt($('#container').attr('data-slider-nbslides'));
				var previs = position-1;  
				if( previs < 0 ) {
					console.error('You should not be there ... prev');
				} else {					
					privates.showSlideByIndex(previs);
				}
			}
		}
		
		
		
		
		$.fn.slider = function( method ) {
		    
		    if ( methods[method] ) {
		      return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
		    } else if ( typeof method === 'object' || ! method ) {
		      return methods.init.apply( this, arguments );
		    } else {
		      $.error( 'Method ' +  method + ' does not exist on jQuery.tooltip' );
		    }    
		  
		  };
		
})(jQuery)

